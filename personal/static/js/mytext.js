$( function() {
    $( "#accordion" ).accordion({
      collapsible: true
    });
  } );

$(document).ready(function(){
$("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".myTable tr").filter(function() {
    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
});
});

function warna(){
    if (document.body.style.backgroundColor==""){
        document.body.style.backgroundColor ="#b3d9ff";
        $(document).ready(function(){
            $("button").hover(function(){
                $(this).css("background-color", "#1a8cff");
            });
        });
    }
    else{
        document.body.style.backgroundColor ="";
        change_font("judul","b3d9ff")
        $(document).ready(function(){
            $("button").click(function(){
                $(this).css("background-color", "#ff1a75");
            });
        });  
    }
}

var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 300);
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}

// BUAT NGATUR FAVORITES
var favorites = 0;
/**Implementasi supaya bintangnya bisa nyala */
function star(the_id){
    var star_url_biru = "https://i.imgur.com/7CeVmZu.png";
    var star_url_kuning = "https://i.imgur.com/SPTSn8Y.png";;
   
    if(document.getElementById(the_id).src==star_url_biru){
        favorites+=1;
        document.getElementById(the_id).src=star_url_kuning;
    }
    else{
        if(favorites>0){
            favorites-=1;
        }
        else{
            favorites=0;
        }
        document.getElementById(the_id).src=star_url_biru;
    }
    document.getElementById("fav").innerHTML = "Favorites: "+favorites;
}

$(document).ready(function(){

    var html_string = "";
    var keyword ="quilting";
    var urls = 'https://www.googleapis.com/books/v1/volumes?q=quilting';
    var counter = 0;

    $('[data-toggle="tooltip"]').tooltip();   
    $(".accordion").on("click", ".accordion-header", function() {
        $(this).toggleClass("active").next().slideToggle();
    });
    $('#search_button').click(function(){
        keyword = $('#search_box').val();
        if (keyword.length!=0){
            html_string = "";
        }
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url:'/getjson/'+keyword,
            crossDomain:true,
            success: function(data){
            console.log(keyword);
                
            var unique_id;
            $.each(data.items,function(i,book){
                unique_id=""+counter;
                counter+=1;
                var star_url = "https://i.imgur.com/7CeVmZu.png";
                html_string+= "<tr>"+
                '<td class = "image">' + "<img src='"+data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>"+
                "<td>"+book.volumeInfo.title+"</td>"+
                 "<td>"+book.volumeInfo.authors+"</td>"+
                 "<td>"+book.volumeInfo.publisher+"</td>"+
                 "<td>"+book.volumeInfo.publishedDate+"</td>"+
                 " <td><img src=\""+star_url+"\" id="+unique_id+" onclick=\"star("+unique_id+")\" style =\"max-width:20px;max-height:20px;\"></td>"+
                 "</tr>";
            })
            $("#start").html(html_string)
        }
    });
});

    /*Get Json dan menampilkan Json pada tabel */
    $.ajax({
        type:'GET',
        dataType: 'json',
        url: '/getjson/quilting',  
        crossDomain: true,
        success: function(data){
            var unique_id;
            $.each(data.items,function(i,book){
                unique_id=""+counter;
                counter+=1;
                var star_url = "https://i.imgur.com/7CeVmZu.png";
                html_string+= "<tr>"+
                '<td class = "image">' + "<img src='"+data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>"+
                "<td>"+book.volumeInfo.title+"</td>"+
                 "<td>"+book.volumeInfo.authors+"</td>"+
                 "<td>"+book.volumeInfo.publisher+"</td>"+
                 "<td>"+book.volumeInfo.publishedDate+"</td>"+
                 " <td><img src=\""+star_url+"\" id="+unique_id+" onclick=\"star("+unique_id+")\" style =\"max-width:20px;max-height:20px;\"></td>"+
                 "</tr>";
            })
            $("#start").html(html_string)
        }
    });
});
