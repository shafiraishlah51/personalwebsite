from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.http import HttpResponse, JsonResponse
from .models import Testimoni
from .forms import Form_Testimoni
import json

response={}
# Create your views here.
def home (request):
    komen = Testimoni.objects.all().order_by('-id')
    response['komen'] = komen
    response['formtesti'] = Form_Testimoni
    html = 'home.html'
    form = Form_Testimoni(request.POST or None)

    if (request.method == 'POST'):
        response['Comment'] = request.POST['Comment']
        save_data = Testimoni(Comment = response['Comment'])
        save_data.save()
        save_data = Testimoni.objects.all().order_by('-id')
        return render(request, html, response)
    return render(request, html, response)
