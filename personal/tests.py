from django.test import TestCase

# Create your tests here.


from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import home
from django.utils import timezone
import time




# Create your tests here.
class PersonalTest(TestCase):

    def test_personal_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)
    
    def test_personal_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)
    
    def test_template_personal(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home.html')

   