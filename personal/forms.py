from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django.db import models
from .models import Testimoni

class Form_Testimoni(forms.Form):
  Comment = forms.CharField(widget=forms.Textarea(attrs={
  'id'    : 'post-text',
  'class'   : 'form-control',
  'required'  : True,
  'placeholder' : 'Write your testimonial here......',
 }), label='', max_length=300)
 
   